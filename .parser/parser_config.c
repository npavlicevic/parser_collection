#include "parser_config.h"

/*
* Function: line_make
* 
* make line instance
*
* return: line
*/
Line *line_make(int line_number, char *text, int text_capacity) {
  Line *this = (Line*)calloc(1, sizeof(Line));
  this->line_number = line_number;
  this->type = LITERAL;
  this->parent = -1;
  this->value.text = (char*)calloc(text_capacity, sizeof(char));
  memcpy(this->value.text, text, text_capacity);
  return this;
}
/*
* Function: line_read
* 
* read line from file
*
* return: line
*/
Line* line_read(FILE *file, int line_number, char *buffer, int text_capacity) {
  int read = 0;
  int i = 0;
  char ch;
  for(;1;) {
    read = fscanf(file, "%c", &ch);
    if(read != 1) {
      return NULL;
    }
    if(ch == '\n') {
      buffer[i] = '\0';
      break;
    }
    buffer[i++] = ch;
  }
  return line_make(line_number, buffer, text_capacity);
}
/*
* Function: line_print
* 
* print line
*
* return: void
*/
void line_print(Line *this) {
  int i = 0;
  int type = this->type;
  printf("line(%d) ", this->line_number);
  for(;type == LITERAL || type == TAG;) {
    if(this->value.text[i] == '\0') {
      printf(" ");
      break;
    }
    printf("%c", this->value.text[i++]);
  }

  if(type == NUMBER) {
    printf("%d ", this->value.number);
  }

  if(type == REAL) {
    printf("%f ", this->value.real);
  }

  #ifdef MORE
    printf("\ndepth -- %d ", this->depth);
    printf("parent -- %d ", this->parent);
    printf("type -- %d ", this->type);
    printf("hash -- %d \n", this->hash);
  #endif

  printf("\n");
}
/*
* Function: line_print_all
* 
* print line all
*
* return: void
*/
void line_print_all(Line *this) {
  int i = 0;
  int type = this->type;
  printf("line(%d) ", this->line_number);
  for(;type == LITERAL || type == TAG;) {
    if(this->value.text[i] == '\0') {
      printf(" ");
      break;
    }
    printf("%c", this->value.text[i++]);
  }

  if(type == NUMBER) {
    printf("%d ", this->value.number);
  }

  if(type == REAL) {
    printf("%f ", this->value.real);
  }

  printf("\ndepth -- %d ", this->depth);
  printf("parent -- %d ", this->parent);
  printf("type -- %d ", this->type);
  printf("hash -- %d ", this->hash);
  printf("parent hash -- %d ", this->parent_hash);
}
/*
* Function: line_print_type
* 
* print line type
*
* return: void
*/
void line_print_type(Line *this) {
  if(this->type == LITERAL) {
    printf("LITERAL ");
  }

  if(this->type == TAG) {
    printf("TAG ");
  }

  if(this->type == NUMBER) {
    printf("NUMBER ");
  }

  if(this->type == REAL) {
    printf("REAL ");
  }
}
/*
* Function: line_table_print
* 
* print lines in table
*
* return: void
*/
void line_table_print(Table *table) {
  int i = 0;
  for(; i < table->capacity; i++) {
    if(table->value[i] == NULL) {
      continue;
    }

    line_print_all((Line*)table->value[i]);
    printf("\n");
  }
}
/*
* Function: line_trim
* 
* trim spaces from line
*
* return: void
*/
void line_trim(Line *this, int text_capacity) {
  int i = 0;

  for(;1; i++) {
    if(this->value.text[i] != ' ') {
      break;
    }
  }

  this->depth = i;
  memcpy(this->value.text, this->value.text + i, text_capacity);
}
/*
* Function: line_hash
* 
* produce line hash from content
*
* return: void
*/
void line_hash(Line *this) {
  this->hash = hash_string(this->value.text, strlen(this->value.text), -1);
}
/*
* Function: line_cast_number
* 
* cast line to number
*
* return: line
*/
Line *line_cast_number(Line *this) {
  Line *another = line_clone_simple(this);

  another->type = NUMBER;
  another->value.number = atoi(this->value.text);

  return another;
}
/*
* Function: line_cast_real
* 
* cast line to real
*
* return: line
*/
Line *line_cast_real(Line *this) {
  Line *another = line_clone_simple(this);

  another->type = REAL;
  another->value.real = atof(this->value.text);

  return another;
}
/*
* Function: line_cast_tag
* 
* cast line to tag
*
* return: line
*/
Line *line_cast_tag(Line *this) {
  this->type = TAG;

  return this;
}
/*
* Function: line_clone_simple
* 
* copy line
*
* return: line
*/
Line *line_clone_simple(Line *this) {
  Line *another = (Line*)calloc(1, sizeof(Line));
  
  another->line_number = this->line_number;
  another->indent_count = this->indent_count;
  another->depth = this->depth;
  another->parent = this->parent;
  another->hash = this->hash;
  another->parent_hash = this->parent_hash;

  return another;
}
/*
* Function: line_max_depth
* 
* maximum depth in configuration file
*
* return: integer
*/
int line_max_depth(Table *table) {
  int i = 0;
  int depth = 0;
  Line *line;

  for(;i < table->capacity; i++) {
    if(table->value[i] == NULL) {
      continue;
    }

    line = (Line*)table->value[i];
    if(line->depth > depth) {
      depth = line->depth;
    }
  }

  return depth;
}
/*
* Function: line_destroy
* 
* destroy line
*
* return: void
*/
void line_destroy(Line **this) {
  if((*this)->type == LITERAL) {
    free((*this)->value.text);
  }
  free(*this);
  *this = NULL;
}
/*
* Function: line_table_destroy
* 
* destroy lines in table
*
* return: void
*/
void line_table_destroy(Table *table) {
  Line *line;
  int i = 0;
  for(;i < table->capacity;i++) {
    if(table->value[i] == NULL) {
      continue;
    }
    line = (Line*)table->value[i];
    line_destroy(&line);
  }
}

#ifndef PARSER_CONFIG_H
#define PARSER_CONFIG_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "table.h"
typedef enum line_type {NUMBER, REAL, LITERAL, COMMENT, TAG} LineType;
typedef union line_value {
  int number;
  double real;
  char *text;
} LineValue;
typedef struct line {
  int line_number;
  int indent_count;
  int depth;
  int parent;
  int hash;
  int parent_hash;
  LineType type;
  LineValue value;
} Line;

Line* line_make(int line_number, char *text, int text_capacity);
Line* line_read(FILE *file, int line_number, char *buffer, int text_capacity);
void line_print(Line *this);
void line_print_all(Line *this);
void line_print_type(Line *this);
void line_table_print(Table *table);
void line_trim(Line *this, int text_capacity);
void line_hash(Line *this);
Line *line_cast_number(Line *this);
Line *line_cast_real(Line *this);
Line *line_cast_tag(Line *this);
Line *line_clone_simple(Line *this);
int line_max_depth(Table *table);
void line_destroy(Line **this);
void line_table_destroy(Table *table);
#endif

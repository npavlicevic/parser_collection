#include "parser_config.h"

int main(int argc, char **args) {
  int i = 0;
  Line *line = NULL;
  Table *table = table_create(2048);
  FILE *file;
  int buffer_capacity = 128;
  char buffer[buffer_capacity];

  if(argc != 2) {
    return 1;
  }

  printf("file name %s \n", args[1]);
  file = fopen(args[1], "r");
  for(;1; i++) {
    line = line_read(file, i, buffer, buffer_capacity);

    if(line == NULL) {
      break;
    }

    table_insert(table, line, hash_mirror_int(&(line->line_number)));
  }

  line_table_print(table);
  line_table_destroy(table);
  table_destroy(&table);
  fclose(file);
  return 0;
}

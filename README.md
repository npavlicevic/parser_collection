## description

a collection of tools to parse a markup language

## language example
```
.root
  .name
    ~ application name
    numericApp
  .include
    ~ include files
    numeric.h
  .optionCount
    ~ number of options
    5
  .argumentCount
    ~ number of arguments
    5
  .inputLength
    ~ number of inputs
    3
  .bufferLength
    ~ output length
    2
  .option
    ~ options listed
    gcdI
    gcdLinearI
    gcdIsSolvableI
    gcdGeneralI
    lcmI
  .returnType
    ~ option return type
    ~ 0 integer
    ~ 1 integer pointer
    0
    1
    0
    1
    0
```
